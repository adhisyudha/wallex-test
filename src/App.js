import React, { Component } from 'react';
import './App.css';
import FormAccount from './FormAccount';
import ListAccount from './ListAccount';

class App extends React.Component {
    constructor(props) {
        super(props);
    };
    state = {
        bankAccount: [],
        mode: 'insert',
        activeId: 0
    };
    componentDidMount() {
        // this.props.onRef(this);
        this.ListAccount.refreshData();

    }
    refreshList() {
        this.ListAccount.refreshData();

        console.log('click');
    }
    renderDataById() {

    }
    changeMode(e) {
        console.log('tes', e.id);
        this.setState({
            mode: e.mode,
            activeId: e.id
        });
        this.FormAccount.renderDataById(e.id);
    }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <FormAccount mode={this.state.mode} refreshList={this.refreshList.bind(this)} onRef={ref => (this.FormAccount = ref)}/>
          <ListAccount changeMode={(value)=> this.changeMode(value)} onRef={ref => (this.ListAccount = ref)}/>
        </header>
      </div>
    );
  }
}

export default App;
