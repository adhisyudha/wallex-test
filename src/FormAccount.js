import React from 'react'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
// import ListAccount from './ListAccount';
import _ from 'lodash';

const styles = theme => ({
    container: {
        // display: 'flex',
        // flexWrap: 'wrap',
        flexGrow:1,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 500,
    },
    colorWarning: {
        backgroundColor: 'orange'+'!important',
        borderColor: 'orange'+'!important',
    },
    bootstrapRoot: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 16,
        padding: '6px 12px',
        border: '1px solid',
        lineHeight: 1.5,
        backgroundColor: '#007bff',
        borderColor: '#007bff',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: '#0069d9',
            borderColor: '#0062cc',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
});

const  type = [
    {
        id:1,
        label:'Individual'
    },
    {
        id:2,
        label:'Company'
    }
];

class FormAccount extends React.Component{
    state = {
        accountName:'',
        accountNumber:'',
        swiftCode:'',
        address:'',
        city:'',
        country:'',
        currency:[],
        currencyVal:'',
        type:'',
        firstName:'',
        lastName:'',
        company:'',
        bankAccount : [],
    };


    constructor(props) {
        super(props);
    };

    // handleChangeAccountName = name => event => {
    //     console.log('name', name)
    //     console.log('event', event)
    //     this.setState({ [name]: event.target.value });
    // };
    // handleChangeAccountNumber = name => event => {
    //     this.setState({ [name]: event.target.value });
    // };
    handleChangeCurrency = name => event => {
        this.setState({ currencyVal: event.target.value });
    };
    handleChange = name => event => {
        console.log('ini event', event)
        this.setState({ [name]: event.target.value});
        // console.log(this.state.type);
    };
    addBankAccount(){
        let data2 = {
            id: this.renderIndex(),
            accountName:this.state.accountName,
            accountNumber:this.state.accountNumber,
            swiftCode:this.state.swiftCode,
            address:this.state.address,
            city:this.state.city,
            country:this.state.country,
            currencyVal:this.state.currencyVal,
            type:this.state.type,
            firstName:this.state.firstName,
            lastName:this.state.lastName,
            company:this.state.company
        };
        //localStorage.setItem('dataAccountBank', JSON.stringify(null));
        var data =this.getDataBankAccount();

        if(data==null) {
            data = [];
        }
        data.push(data2);

        localStorage.setItem('dataAccountBank', JSON.stringify(data));
        // setTimeout(function(){console.log('set timeout',localStorage.getItem('dataAccountBank'))}, 5000)
        // console.log('ini data dari local storage',localStorage.getItem('dataAccountBank'));
        // console.log('data state bank akun', this.state.bankAccount);
        // this.setState({
        //     bankAccount: data
        // }
        this.tryRefresh();
        this.resetData();


    }

    getDataBankAccount() {
        const store = JSON.parse(localStorage.getItem('dataAccountBank'));
        return store;
    }

    renderIndex() {
        const store = this.getDataBankAccount();
        if(store || store!== null) {
            const index = _.maxBy(store, 'id');
            return index.id + 1;
        }
        return 1;
    }

    componentDidMount(){
        this.props.onRef(this)
        // localStorage.setItem('dataAccountBank', JSON.stringify(null));
        // console.log()
        //
        // console.log('store', this.getDataBankAccount());
        this.setState({
            bankAccount: this.getDataBankAccount()
        });

        return fetch('https://restcountries.eu/rest/v2/all')
            .then((response) => response.json())
            .then((responseJson) => {
                this.currency = responseJson;
                this.setState({
                    currency: responseJson,
                }, function(){

                });
                // console.log(this.currency);
            })
            .catch((error) =>{
                console.error(error);
            });
    }
    componentWillUnmount() {
        this.props.onRef(undefined)
    }
    tryRefresh() {
        this.props.refreshList();
    }
    renderDataById(id) {
        var id = id;

        const store = this.getDataBankAccount();
        console.log('id', id);
        let data2= [];
        const data = _.each(store, function (item,index) {
            console.log(item.id)
            if(item.id === id){
                data2 = item;
            }
        });

        if(data2) {
            this.setState({
                id:data2.id,
                accountName:data2.accountName,
                accountNumber:data2.accountNumber,
                swiftCode:data2.swiftCode,
                address:data2.address,
                city:data2.city,
                country:data2.country,
                // currency:[],
                currencyVal:data2.currencyVal,
                type:data2.type,
                firstName:data2.firstName,
                lastName:data2.lastName,
                company:data2.company,
            })
        }

    }
    updateBankAccount(){
        const  store = this.getDataBankAccount();
        var temp = this;
        const  data = _.map(store, function (item, index) {
            if(item.id== temp.state.id){
                item.accountName = temp.state.accountName;
                item.accountNumber=temp.state.accountNumber;
                item.swiftCode=temp.state.swiftCode;
                item.address=temp.state.address;
                item.city=temp.state.city;
                item.country=temp.state.country;
                item.currencyVal=temp.state.currencyVal;
                item.type=temp.state.type;
                item.firstName=temp.state.firstName;
                item.lastName=temp.state.lastName;
                item.company=temp.state.company;
            }
            return item;
        });

        localStorage.setItem('dataAccountBank', JSON.stringify(data));
        this.tryRefresh();
        this.resetData();
    }

    resetData(){
        this.setState({
            accountName:'',
            accountNumber:'',
            swiftCode:'',
            address:'',
            city:'',
            country:'',
            currencyVal:'',
            type:'',
            firstName:'',
            lastName:'',
            company:'',
        })

    }
    render(){
        const { classes } = this.props;
        let typeComponent;
        if (this.state.type == 1) {
            typeComponent = <div><TextField
                id="standard-name"
                label="Firs Name"
                className={classes.textField}
                value={this.state.firstName}
                onChange={this.handleChange('firstName')}
                margin="normal"
            />
            <TextField
            id="standard-name"
            label="Last Name"
            className={classes.textField}
            value={this.state.lastName}
            onChange={this.handleChange('lastName')}
            margin="normal"
                /></div>;
        } else if(this.state.type == 2) {
            typeComponent = <div>
                <TextField
                id="standard-name"
                label="Company"
                className={classes.textField}
                value={this.state.company}
                onChange={this.handleChange('company')}
                margin="normal"
            />
            </div>;
        }else{

            typeComponent = '';
        }
        return (
            <div>
                <form className={classes.container} noValidate autoComplete="off">
                    {/*<p className={classes.p}>{this.state.accountName}</p>*/}
                    <TextField
                        id="standard-name"
                        label="Account Name"
                        className={classes.textField}
                        value={this.state.accountName}
                        onChange={this.handleChange('accountName')}
                        // onChange={console.log('asd')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        type="number"
                        label="Account Number"
                        className={classes.textField}
                        value={this.state.accountNumber}
                        onChange={this.handleChange('accountNumber')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Swift Code"
                        className={classes.textField}
                        value={this.state.swiftCode}
                        onChange={this.handleChange('swiftCode')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Address"
                        className={classes.textField}
                        value={this.state.address}
                        onChange={this.handleChange('address')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="City"
                        className={classes.textField}
                        value={this.state.city}
                        onChange={this.handleChange('city')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Country"
                        className={classes.textField}
                        value={this.state.country}
                        onChange={this.handleChange('country')}
                        margin="normal"
                    />
                    <div>
                        <TextField
                            id="standard-select-currency"
                            select
                            label="Currency"
                            className={classes.textField}
                            value={this.state.currencyVal}
                            onChange={this.handleChangeCurrency('currency')}
                            SelectProps={{
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                            helperText="Please select your currency"
                            margin="normal"
                        >
                            {this.state.currency.map((option,idx) => (
                                <MenuItem key={idx} value={option.currencies[0].code}>
                                    {option.currencies[0].code} - {option.currencies[0].name}
                                </MenuItem>
                            ))}
                        </TextField>
                        <TextField
                            id="standard-select-type"
                            select
                            label="Type"
                            className={classes.textField}
                            value={this.state.type}
                            onChange={this.handleChange('type')}
                            SelectProps={{
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                            helperText="Please select your type"
                            margin="normal"
                        >
                            {type.map((option,idx) => (
                                <MenuItem key={idx} value={option.id}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </div>
                    {typeComponent}
                    <div>
                        <Button
                            variant="contained"
                            color="primary"
                            disableRipple
                            className={classNames(classes.margin, classes.bootstrapRoot, classes.colorWarning)}
                            onClick={()=>this.resetData()}
                        >
                            Reset
                        </Button>
                        {(this.props.mode == 'insert')?
                        <Button
                            variant="contained"
                            color="primary"
                            disableRipple
                            className={classNames(classes.margin, classes.bootstrapRoot)}
                            onClick={()=>this.addBankAccount()}
                        >
                            Add
                        </Button>
                        :
                        <Button
                            variant="contained"
                            color="primary"
                            disableRipple
                            className={classNames(classes.margin, classes.bootstrapRoot)}
                            onClick={()=>this.updateBankAccount()}
                        >
                            Edit
                        </Button>
                        }
                        <Button
                            variant="contained"
                            color="primary"
                            disableRipple
                            className={classNames(classes.margin, classes.bootstrapRoot)}
                            onClick={()=>this.tryRefresh()}
                        >
                            refresh
                        </Button>
                    </div>
                </form>
            </div>

        )
    }
}

export default withStyles(styles)(FormAccount);
