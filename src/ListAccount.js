import React from 'react'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import _ from 'lodash';

let id = 0;
function createData(accountName, accountNumber, swiftCode, address, city, country, currencyVal, type, firstName, lastName, company) {
    id += 1;
    return { id, accountName, accountNumber, swiftCode, address, city, country, currencyVal, type, firstName, lastName, company};
}

const styles = theme => ({
    root: {
        width: '80%',
        marginTop: theme.spacing.unit * 5,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    bootstrapRoot: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 10,
        padding: '3px 3px',
        border: '1px solid',
        lineHeight: 1.5,
        backgroundColor: '#007bff',
        borderColor: '#007bff',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: '#0069d9',
            borderColor: '#0062cc',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
    danger:{
        backgroundColor: 'red'+'!important',
        borderColor: 'red'+'!important',
    },
    success:{
        backgroundColor: 'green'+'!important',
        borderColor: 'green'+'!important',
    }
});
class ListAccount extends React.Component{
    state = {
        _bankAccount: [],
        type:['','individual', 'company']
    };

    componentDidMount() {
        this.props.onRef(this)
    }

    getDataBankAccount() {
        const store = JSON.parse(localStorage.getItem('dataAccountBank'));
        if(store|| store!=null){
            return store;
        }
        return [];

    }

    refreshData() {
        console.log('store2', this.getDataBankAccount());
        // this.state.bankAccount.push(this.getDataBankAccount());
        this.setState({
            _bankAccount: this.getDataBankAccount()
        });
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    deleteData(e){
        console.log(e.row.id);
        const iddelete = e.row.id;
        const data = this.getDataBankAccount();
        const newData = _.filter(data, function (item, id) {
           return item.id !== iddelete;
        });
        localStorage.setItem('dataAccountBank', JSON.stringify(newData));
        this.refreshData();
    }

    editData(e){
        console.log(e.row.id);
        const id = e.row.id;
        const data = {
            id: id,
            mode: 'edit'
        }
        this.props.changeMode(data);
    }

    render(){
        const { classes } = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">ID</TableCell>
                            <TableCell align="right">Account Name</TableCell>
                            <TableCell align="right">Account Number</TableCell>
                            <TableCell align="right">Swift Code</TableCell>
                            <TableCell align="right">Addresss</TableCell>
                            <TableCell align="right">City</TableCell>
                            <TableCell align="right">Country</TableCell>
                            <TableCell align="right">Courrency</TableCell>
                            <TableCell align="right">Type</TableCell>
                            <TableCell align="right">First Name</TableCell>
                            <TableCell align="right">Last Name</TableCell>
                            <TableCell align="right">Company</TableCell>
                            <TableCell align="right">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state._bankAccount.map((row, id) => (
                            <TableRow key={id}>
                                <TableCell component="th" scope="row">{row.id}</TableCell>
                                <TableCell align="right">{row.accountName}</TableCell>
                                <TableCell align="right">{row.accountNumber}</TableCell>
                                <TableCell align="right">{row.swiftCode}</TableCell>
                                <TableCell align="right">{row.address}</TableCell>
                                <TableCell align="right">{row.city}</TableCell>
                                <TableCell align="right">{row.country}</TableCell>
                                <TableCell align="right">{row.currencyVal}</TableCell>
                                <TableCell align="right">{this.state.type[row.type]}</TableCell>
                                <TableCell align="right">{row.firstName}</TableCell>
                                <TableCell align="right">{row.lastName}</TableCell>
                                <TableCell align="right">{row.company}</TableCell>

                                <TableCell align="right">
                                    <div>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disableRipple
                                            className={classNames(classes.margin, classes.bootstrapRoot, classes.danger)}
                                            onClick={()=>this.deleteData({row})}
                                        >
                                            Delete{row.id}
                                        </Button>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disableRipple
                                            className={classNames(classes.margin, classes.bootstrapRoot, classes.success)}
                                            onClick={()=>this.editData({row})}
                                        >
                                            Edit
                                        </Button>
                                    </div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

export default withStyles(styles)(ListAccount);